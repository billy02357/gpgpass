POSIX shell script for managing passwords

# Requirements

- ```dmenu```

# Install
```
git clone https://github.com/Billy02357/gpgpass
cp gpgpass/gpgpass ~/.local/bin/gpgpass
rm -rf gpgpass
```

Password files (.gpg) are stored in ~/.config/passwds; the first time you run the script you'll have to run 
```gpgpass -n```
to create a new password

# Usage

```
$ gpgpass -h

Usage: gpgpass <option>
Options:
        No option       Prompt the dmenu and copy selected password
        -h              Print this menu
        -c              Decrypt password file and copy to clipboard
        -n              Create a new password
        -r              Remove an existent password
```
